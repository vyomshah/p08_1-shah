//
//  FirstViewController.swift
//  p08-Shah
//
//  Created by Pranav Bhandari on 5/10/17.
//  Copyright © 2017 Vyom Shah. All rights reserved.
//

import UIKit
var arr : [String] = []

class FirstViewController: UIViewController, UITableViewDelegate {
    
    @IBOutlet weak var tableView : UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        if (UserDefaults.standard.object(forKey: "todo") != nil){
           // print("---------")
            arr = UserDefaults.standard.object(forKey: "todo") as! [String]
        }
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
        print("------",arr.count)
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
        cell.textLabel!.text = arr[indexPath.row]
        return cell
    }

    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAtIndexPath indexPath: IndexPath) -> Bool {
        return true
    }
    //Make a delete button if the item is added in the list. 
    func tableView(_ tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            arr.remove(at: indexPath.row)
            UserDefaults.standard.set(arr, forKey: "todo")
            tableView.reloadData()
        }
    }


}


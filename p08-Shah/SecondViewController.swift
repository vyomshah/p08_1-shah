//
//  SecondViewController.swift
//  p08-Shah
//
//  Created by Pranav Bhandari on 5/10/17.
//  Copyright © 2017 Vyom Shah. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var newItem : UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.newItem.delegate = self
    }
    
    @IBAction func addItem(_ sender: AnyObject) {
        arr.append(newItem.text!)
        UserDefaults.standard.set(arr, forKey: "todo")
        newItem.text = ""
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
 

    }

